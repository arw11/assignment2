//This program allows the user to choose between a natural or zero first derivative spline. It then asks for the number of iterations between each data point to perform the spline. Finally, it writes the data to a file called "interpolation.txt". I plotted the data on excel by copying and pasting the .txt file directly into excel. 
#include <iostream>
#include <fstream>

using namespace std;

//A NOTE FROM ANDREW WARWICK; THIS TRIDIAGONAL MATRIX SOLVER WAS SOURCED FROM https://en.wikibooks.org/wiki/Algorithm_Implementation/Linear_Algebra/Tridiagonal_matrix_algorithm (accessed 15th November 2015) ; I TAKE NO CREDIT FOR THIS FUNCTION IN MY CODE

void solve(double* a, double* b, double* c, double* d, int n) {
    /*
    // n is the number of unknowns

    |b0 c0 0 ||x0| |d0|
    |a1 b1 c1||x1|=|d1|
    |0  a2 b2||x2| |d2|

    1st iteration: b0x0 + c0x1 = d0 -> x0 + (c0/b0)x1 = d0/b0 ->

        x0 + g0x1 = r0               where g0 = c0/b0        , r0 = d0/b0

    2nd iteration:     | a1x0 + b1x1   + c1x2 = d1
        from 1st it.: -| a1x0 + a1g0x1        = a1r0
                    -----------------------------
                          (b1 - a1g0)x1 + c1x2 = d1 - a1r0

        x1 + g1x2 = r1               where g1=c1/(b1 - a1g0) , r1 = (d1 - a1r0)/(b1 - a1g0)

    3rd iteration:      | a2x1 + b2x2   = d2
        from 2st it. : -| a2x1 + a2g1x2 = a2r2
                       -----------------------
                       (b2 - a2g1)x2 = d2 - a2r2
        x2 = r2                      where                     r2 = (d2 - a2r2)/(b2 - a2g1)
    Finally we have a triangular matrix:
    |1  g0 0 ||x0| |r0|
    |0  1  g1||x1|=|r1|
    |0  0  1 ||x2| |r2|

    Condition: ||bi|| > ||ai|| + ||ci||

    in this version the c matrix reused instead of g
    and             the d matrix reused instead of r and x matrices to report results
    Written by Keivan Moradi, 2014
    */
    n--; // since we start from x0 (not x1)
    c[0] /= b[0];
    d[0] /= b[0];

    for (int i = 1; i < n; i++) {
	c[i] /= b[i] - a[i]*c[i-1];
        d[i] = (d[i] - a[i]*d[i-1]) / (b[i] - a[i]*c[i-1]);
    }

    d[n] = (d[n] - a[n]*d[n-1]) / (b[n] - a[n]*c[n-1]);

    for (int i = n; i-- > 0;) {
        d[i] -= c[i]*d[i+1];
    }
}

int main()
{		
	double x[1000],y[1000]; //Declare 1KB of space for x and y data
	ifstream ifs; //Opens input file stream named 'ifs'
	ifs.open("data.txt"); //Reads the file called 'data.txt' in the same folder
	
	if(!ifs) //The condition here is that ifs doesn't open
	{
	 cout << "Couldn't read file";
	}
	int i=0;
	while(ifs >> x[i] >> y[i])
	{ 
	i++;
	}
	
	int s=i; //s is the number of data points contained in data.txt
	
	char choice;
	cout << "Choose 'n' for natural spline and 'z' for zero first derivative at the end of each boundary ";
	cin >> choice;
	double a[s],b[s],c[s],d[s];	

	if(choice=='n') //Natural Spline boundary conditions
	{
		a[0]=0;
		a[s-1]=0;
		b[0]=1;
      	        b[s-1]=1;
		c[0]=0;
	        c[s-1]=0;
		d[0]=0;
       	        d[s-1]=0;
	}
	
	else if(choice=='z') //Zero first derivative boundary conditions
	{
		a[0]=0;
		a[s-1]=(x[s-2]-x[s-1])/6;
		b[0]=(x[1]-x[0])/3;
		b[s-1]=(x[s-2]-x[s-1])/3;
		c[0]=(x[1]-x[0])/6;
		c[s-1]=0;
		d[0]=(y[1]-y[0])/(x[1]-x[0]);
		d[s-1]=(y[s-1]-y[s-2])/(x[s-1]-x[s-2]);
	}
	
	//Input the a b c and F (which here is labelled d) terms from the master equation
	for(int j=1; j<=(s-2); j++)
	{
	 a[j]=(x[j]-x[j-1])/6;
	}

	for(int k=1; k<=(s-2); k++)
	{
	 b[k]=(x[k+1]-x[k-1])/3;
	}

	for(int l=1; l<=(s-2); l++)
	{
	 c[l]=(x[l+1]-x[l])/6;
	}
	
	for(int m=1; m<=(s-2); m++)
	{
	 d[m]=((y[m+1]-y[m])/(x[m+1]-x[m]))-((y[m]-y[m-1])/(x[m]-x[m-1]));
	}

	int N;
	double X,Y;
	solve(a,b,c,d,s); // This function replaces 'd' (column vector F) with the y'' values	
	cout<< "Please insert number of iterations between each point: ";
	cin >> N; //Allows user to choose how dense he/she wants the plot to be

	ofstream interpolation;
	interpolation.open("interpolation.txt");//Spline data will be written to this text file

	 for(int n=0; n<(s-1) ; n++) // s-1 to preent overshooting the last data point
		{
		  X=x[n]; //Ensures that the variable X starts from the beginning of a given interval
		  for(int p=0; p<=N ; p++)
		   {
		    double increment=(x[n+1]-x[n])/N; //Divides the interval into N pieces
		    //Calculates A,B,C,D coefficients from page 10 in the lecture 2 slides
		    double A=((x[n+1]-X)/(x[n+1]-x[n]));
		    double B= 1 - A;
		    double C= (((A*A*A)-A)*(x[n+1]-x[n])*(x[n+1]-x[n]))/6;
		    double D= (((B*B*B)-B)*(x[n+1]-x[n])*(x[n+1]-x[n]))/6;
		    double Y= (A*y[n])+(B*y[n+1])+(C*d[n])+(D*d[n+1]);
		    interpolation << X << '\t' << Y << endl;
		    X = X+increment;
		   }
		}	     
	return 0;
} 			

//This program asks for the number of iterations between points and writes the resulting data into a text file called "lininterpol.txt". I also used excel to plot this data.
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
        double x[1000],y[1000];
        ifstream ifs;
        ifs.open("data.txt");

        if(!ifs)
        {
         cout << "Couldn't read file";
        }
        int i=0;
        while(ifs >> x[i] >> y[i])
        {
        i++;
        }

        int s=i; //s*s is the size of the matrix
	int N;
        double X,Y;
        cout<< "Please insert number of iterations between each point: ";
        cin >> N;

        ofstream linint;
        linint.open("lininterpol.txt");

	for(int n=0; n<(s-1) ; n++) // s-1 to preent overshooting the last data point
                {
                  X=x[n];
                  for(int p=0; p<=N ; p++)
                   {
                    double increment=(x[n+1]-x[n])/N;
                    double A=((x[n+1]-X)/(x[n+1]-x[n]));
                    double B= 1 - A;
                    double Y= (A*y[n])+(B*y[n+1]);
                    linint << X << '\t' << Y << endl;
                    X = X+increment;
                   }
                }
return 0;

}
